#include "Robot.h"

int main ()
{
    conectar( "/dev/ttyACM0", MODO_EJECUTAR );
    
    pinzaAbrir();
    alturaAbsoluta( 5 );
    
    while ( 1 )
    {
        motorAbsoluta( MOTOR1, 90 );
        motorAbsoluta( MOTOR2, -90 );

        alturaAbsoluta( 0 );
        pinzaCerrar();
        alturaAbsoluta( 5 );

        motorAbsoluta( MOTOR1, -90 );
        motorAbsoluta( MOTOR2, 90 );

        alturaAbsoluta( 2 );
        pinzaAbrir();
        alturaAbsoluta( 0 );
    }
    desconectar();
    
    return 0;
}
