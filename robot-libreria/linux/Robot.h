/*
 * Robot.h
 *
 *  Creado: 13/5/19
 *
 *  Autor: Jorge Rosas
 */

#ifndef ROBOT_H
#define ROBOT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#define MOTOR1      1
#define MOTOR2      2
#define MOTOR3      3

#define MODO_EJECUTAR   'e'
#define MODO_GUARDAR    'g'

#define BAUD        9600
#define BUF_MAX     256

#define MSJ_ERROR   "No hay conexión con el robot scara, comando %d\n"
#define MSJ_MARGEN  "Motor %d va desde %sº hasta %sº, comando %d\n"
#define MSJ_PINZA   "La altura de la pinza va desde %sº hasta %sº, comando %d\n"

#define M1_MAX      90
#define M1_MIN      -90

#define M2_MAX      135
#define M2_MIN      -135

#define M3_MAX      90
#define M3_MIN      -90

#define PINZA_MAX   5
#define PINZA_MIN   0

void conectar           ( char*, char );
void desconectar        ();

void motorRelativa      ( int, int );
void motorAbsoluta      ( int, int );

void alturaRelativa     ( int );
void alturaAbsoluta     ( int );

void pinzaSubir         ();
void pinzaBajar         ();

void pinzaAbrir         ();
void pinzaCerrar        ();

void espera             ( int );
void parametrosRelativa ( int, int, int, int, int );
void parametrosAbsoluta ( int, int, int, int, int );

#endif /* ROBOT_H */