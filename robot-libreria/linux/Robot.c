#include "Robot.h"
#include "arduino-serial-lib.h"

int i = 1;

int m1 = 0;
int m2 = 0;
int m3 = 0;
int al = 0;
int ab = 0;

int fd = -1;

int modo = 0;

void conectar ( char* puerto_serial, char m )
{
    modo = m;
    
    /* Se habre la conexion con el puerto */
    fd = serialport_init( puerto_serial, BAUD );
    
    /* -1 indica error */
    if ( fd == -1 )
        printf( "Error iniciando el puerto %s, verifique que el robot scara se encuentre conectado al mismo\n", puerto_serial );
    else
        serialport_flush( fd );
}

void desconectar ()
{
    if ( modo == MODO_GUARDAR )
        serial_enviar( "r:r\n" );
    else
        serial_enviar( "f:f\n" );
    
    serialport_close( fd );
}

void enviar_parametros ( int ang1, int ang2, int ang3, int altura, int abrir )
{
    char str[50];
    sprintf( str, "%c:%d:%d:%d:%d:%d\n", modo, ang1, ang2, ang3, altura, abrir );
    serial_enviar( str );
}

void espera ( int tiempo )
{
    char str[10];
    sprintf( str, "t:%c:%d\n", modo, tiempo );
    serial_enviar( str );
}

void serial_enviar ( char* str )
{
    printf( str );
    if ( fd == -1 ) { printf( MSJ_ERROR, i ); i++; return; }
    // Se escribe el string en el puerto
    serialport_write( fd, str );
    
    // Se crea un bufer para recibir información
    char buf[] = "--";
    // Mientras no reciba ! que siga esperando, Arduino sigue procesando el mensaje
    while ( strstr( buf, "!" ) == NULL )
        // Se escribe en el buf lo que se recibió de arduino
        serialport_read_until( fd, buf, '!', 2, 10 );
}

void motorRelativa ( int motor, int grados )
{
    if ( motor == MOTOR1 )
    {
        int nueva = m1 + grados;
        if ( nueva > M1_MAX || nueva < M1_MIN )
        {
            printf( MSJ_MARGEN, MOTOR1, M1_MIN, M1_MAX, i );
            i++;
            return;
        }
        m1 = nueva;
        enviar_parametros( grados, 0, 0, al, ab );
    }
    else if ( motor == MOTOR2 )
    {
        int nueva = m2 + grados;
        if ( nueva > M2_MAX || nueva < M2_MIN )
        {
            printf( MSJ_MARGEN, MOTOR2, M2_MIN, M2_MAX, i );
            i++;
            return;
        }
        m2 = nueva;
        enviar_parametros( 0, grados, 0, al, ab );
    }
    else if ( motor == MOTOR3 )
    {
        int nueva = m3 + grados;
        if ( nueva > M3_MAX || nueva < M3_MIN )
        {
            printf( MSJ_MARGEN, MOTOR3, M3_MIN, M3_MAX, i );
            i++;
            return;
        }
        m3 += grados;
        enviar_parametros( 0, 0, m3, al, ab );
    }
    
    i++;
}

void motorAbsoluta ( int motor, int grados )
{
    int dif;
    if ( motor == MOTOR1 )
    {
        if ( grados < M1_MIN || grados > M1_MAX )
        {
            printf( MSJ_MARGEN, MOTOR1, M1_MIN, M1_MAX, i );
            i++;
            return;
        }
        dif = grados - m1;
        enviar_parametros( dif, 0, 0, al, ab );
        m1 += dif;
    }
    else if ( motor == MOTOR2 )
    {
        if ( grados < M2_MIN || grados > M2_MAX )
        {
            printf( MSJ_MARGEN, MOTOR2, M2_MIN, M2_MAX, i );
            i++;
            return;
        }
        dif = grados - m2;
        enviar_parametros( 0, dif, 0, al, ab );
        m2 += dif;
    }
    else if ( motor == MOTOR3 )
    {
        if ( grados < M3_MIN || grados > M3_MAX )
        {
            printf( MSJ_MARGEN, MOTOR3, M3_MIN, M3_MAX, i );
            i++;
            return;
        }
        m3 = grados;
        enviar_parametros( 0, 0, m3, al, ab );
    }
    
    i++;
}

void alturaRelativa ( int altura )
{
    int nueva = al + altura;
    if ( nueva < PINZA_MIN || nueva > PINZA_MAX ) { printf( MSJ_PINZA, PINZA_MIN, PINZA_MAX ); i++; return; }
    
    al += altura; enviar_parametros( 0, 0, m3, al, ab );
    i++;
}

void alturaAbsoluta ( int altura )
{
    if ( altura < PINZA_MIN || altura > PINZA_MAX ) { printf( MSJ_PINZA, PINZA_MIN, PINZA_MAX ); i++; return; }
    
    al = altura; enviar_parametros( 0, 0, m3, al, ab );
    i++;
}

void pinzaSubir ()
{
    al = PINZA_MAX; enviar_parametros( 0, 0, m3, al, ab );
    i++;
}

void pinzaBajar ()
{
    al = PINZA_MIN; enviar_parametros( 0, 0, m3, al, ab );
    i++;
}

void pinzaAbrir ()
{
    ab = 1; enviar_parametros( 0, 0, m3, al, ab );
    i++;
}

void pinzaCerrar ()
{
    ab = 0; enviar_parametros( 0, 0, m3, al, ab );
    i++;
}

void parametrosRelativa ( int motor1, int motor2, int motor3, int altura, int abierto )
{
    int nuevaM1 = m1 + motor1;
    int nuevaM2 = m2 + motor2;
    int nuevaM3 = m3 + motor3;
    
    if ( nuevaM1 > M1_MAX || nuevaM1 < M1_MIN )
    {
        printf( MSJ_MARGEN, MOTOR1, M1_MIN, M1_MAX, i );
        i++;
        return;
    }
    if ( nuevaM2 > M2_MAX || nuevaM2 < M2_MIN )
    {
        printf( MSJ_MARGEN, MOTOR2, M2_MIN, M2_MAX, i );
        i++;
        return;
    }
    if ( nuevaM3 > M3_MAX || nuevaM3 < M3_MIN )
    {
        printf( MSJ_MARGEN, MOTOR3, M3_MIN, M3_MAX, i );
        i++;
        return;
    }
    
    m1 = nuevaM1;
    m2 = nuevaM2;
    m3 += motor3;
    
    int nuevaAlt = al + altura;
    
    if ( nuevaAlt < PINZA_MIN || nuevaAlt > PINZA_MAX ) { printf( MSJ_PINZA, PINZA_MIN, PINZA_MAX ); i++; return; }
    
    altura += altura;
    
    enviar_parametros( m1, m2, m3, al, ab );
    i++;
}

void parametrosAbsoluta ( int motor1, int motor2, int motor3, int altura, int abierto )
{
    if ( motor1 < M1_MIN || motor1 > M1_MIN )
    {
        printf( MSJ_MARGEN, MOTOR1, M1_MIN, M1_MAX, i );
        i++;
        return;
    }
    if ( motor2 < M2_MIN || motor2 > M2_MIN )
    {
        printf( MSJ_MARGEN, MOTOR2, M2_MIN, M2_MAX, i );
        i++;
        return;
    }
    if ( motor3 < M3_MIN || motor3 > M3_MIN )
    {
        printf( MSJ_MARGEN, MOTOR3, M3_MIN, M3_MAX, i );
        i++;
        return;
    }
    
    int dif1 = motor1 - m1;
    m1 += dif1;
    int dif2 = motor2 - m2;
    m2 += dif2;
    m3 = motor3;
    
    if ( altura < PINZA_MIN || altura > PINZA_MAX ) { printf( MSJ_PINZA, PINZA_MIN, PINZA_MAX ); i++; return; }
    al = altura;
    
    enviar_parametros( dif1, dif2, m3, al, ab );
    
    i++;
}