/*
 * Robot.cpp
 *
 *  Creado: 19/12/18
 *
 *  Autor: Jorge
 */

#include "Robot.h"

HANDLE hSerial;
COMSTAT status;
PDWORD errors;

int x = 0;
int y = 0;
int an = 0;
int al = 0;
int ab = 0;

void conectar ( char *puerto )
{
    // Creando puerto
    hSerial = CreateFile( static_cast<LPCSTR> ( puerto ),
            GENERIC_READ | GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            NULL );
    
    if ( hSerial == INVALID_HANDLE_VALUE )
    {
        if ( GetLastError() == ERROR_FILE_NOT_FOUND )
            cout << "ERROR: Puero serial no existe" << endl;
        else
            cout << "ERROR: No se puedo conectar con el puerto" << endl;
    }
    else
    {
        // Colocando parametros del puerto
        DCB dcbSerialParams = { 0 };
        dcbSerialParams.DCBlength = sizeof( dcbSerialParams );

        if ( !GetCommState( hSerial, &dcbSerialParams ) )
            cout << "ERROR: No se pudo obtener la configuración" << endl;
        else
        {
            dcbSerialParams.BaudRate = CBR_9600;
            dcbSerialParams.ByteSize = 8;
            dcbSerialParams.StopBits = ONESTOPBIT;
            dcbSerialParams.Parity = NOPARITY;
            dcbSerialParams.fDtrControl = DTR_CONTROL_ENABLE;

            if ( !SetCommState( hSerial, &dcbSerialParams ) )
                cout << "ERROR: No se puedo configurar el puerto" << endl;
            else
            {
                // Colocando tiempos de espera
                COMMTIMEOUTS timeouts = { 0 };

                timeouts.ReadIntervalTimeout = 50;
                timeouts.ReadTotalTimeoutConstant = 50;
                timeouts.ReadTotalTimeoutMultiplier = 10;
                timeouts.WriteTotalTimeoutConstant = 50;
                timeouts.WriteTotalTimeoutMultiplier = 10;

                if ( !SetCommTimeouts( hSerial, &timeouts ) )
                    cout << "ERROR: No se pudo establecer tiempos de espera" << endl;
                else
                {
                    // Limpiar Tx Rx
                    PurgeComm( hSerial, PURGE_RXCLEAR | PURGE_TXCLEAR );
                    // Esperar a que Arduino este listo para recibir su primer instrucción
                    Sleep( ARDUINO_WAIT_TIME );
                }
            }
        }
    }
}

// Retorna 0 si aún no se recibió una respuesta del Arduino
bool recibido ()
{
    char buffer[ MAX_DATA_LENGTH + 1 ] = { 0 };
    DWORD bytesRead;
    unsigned int toRead;
    
    ClearCommError( hSerial, errors, &status );
    
    if ( status.cbInQue > 0 )
    {
        if ( status.cbInQue > MAX_DATA_LENGTH )
            toRead = MAX_DATA_LENGTH;
        else
            toRead = status.cbInQue;
    }
    
    if ( ReadFile( hSerial, buffer, toRead, &bytesRead, NULL ) )
        return bytesRead;
    
    return 0;
}

// Escribe información por el puerto serial
void escribir ( char *buffer, unsigned int buf_size )
{
    DWORD bytesSend;
    
    if ( !WriteFile( hSerial, ( void* ) buffer, buf_size, &bytesSend, 0 ) )
        cout << "ERROR: Escribiendo puerto" << endl;
}

/* Aruino espera una linea como la siguiente: m1,m2,angulo,altura,abierto
 *
 * m1: Cuantos grados se mueve el motor 1
 * m2: Cuantos grados se mueve el motor 2
 * angulo: El ángulo en que se debe posicionar la pinza
 * altura: La altura en la que se debe posicionar la pinza
 * abiero:
 *      - 0 se cierra la pinza
 *      - 1 se abre la pinza
 *
 * m1 y m2 son movimientos relativos a la posición actual, el resto son coordenadas o instrucciones absolutas
 * */
void enviarComandos ( int m1, int m2, int angulo, int altura, int abierto )
{
    x = m1;
    y = m2;
    an = angulo;
    al = altura;
    ab = abierto;
    
    string m = to_string( m1 ) +
        string( "," ) +
        to_string( m2 ) +
        string( "," ) +
        to_string( angulo ) +
        string( "," ) +
        to_string( altura ) +
        string( "," ) +
        to_string( abierto );
    
    char *c = new char[ m.size() + 1 ];
    strcpy( c, m.c_str() );
    
    escribir( c, MAX_DATA_LENGTH );
    
    while ( !recibido() );
    
    delete[] c;
}

void motor1( int p ) { enviarComandos( p, 0, an, al, ab ); }
void motor2( int p ) { enviarComandos( 0, p, an, al, ab ); }
void motores( int p1, int p2 ) { enviarComandos( p1, p2, an, al, ab ); }
void altura( int p ) { al = p; enviarComandos( 0, 0, an, al, ab ); }
void rotar( int p ) { an = p; enviarComandos( 0, 0, an, al, ab ); }
void subir() { ab = 1; enviarComandos( 0, 0, an, al, ab ); }
void bajar() { ab = 0; enviarComandos( 0, 0, an, al, ab ); }
void punta( int p1, int p2, int p3 ) { an = p1; al = p2; ab = p3; enviarComandos( 0, 0, an, al, ab ); }

void cerrar ()
{
    // Cerrando puerto
    CloseHandle( hSerial );
}