#include "Robot.h"

int main ()
{
    conectar( "COM3" );
    
    while ( true )
    {
        punta( 0, 5, 0 );
        motor2( 90 );
        punta( 0, 0, 1 );
        altura( 5 );

        motores( -90, -135 );
        punta( 0, 0, 0 );
        altura( 5 );

        motores( 90, 45 );

        motores( -90, -45 );
        punta( 0, 0, 1 );
        altura( 5 );

        motores( 180, -45 );
        punta( 0, 0, 1 );
        altura( 5 );

        motores( -90, 90 );
    }
    
    cerrar();
    
    return 0;
}