/*
 * Robot.h
 *
 *  Creado: 19/12/18
 *
 *  Autor: Jorge
 */

#ifndef ROBOT_H
#define ROBOT_H

#include <iostream>
#include <windows.h>

#define ARDUINO_WAIT_TIME   2000
#define MAX_DATA_LENGTH     255

using namespace std;

void conectar ( char* );
void motor1( int );
void motor2( int );
void motores( int, int );
void altura( int );
void rotar( int );
void subir();
void bajar();
void punta( int, int, int );
//void enviarComandos ( int, int, int, int, int );
void cerrar ();

#endif /* ROBOT_H */