/* Creado: 21-12-2018
 * Autor: Jorge Rosas
 * Version: 0.4
 * 
 * Conectar motores a CNC v3:
 * 
 * Motor grande en X, desde arriba hasta abajo:
 *    Verde
 *    Negro
 *    Rojo
 *    Azul0
 * 
 * Motor chico en Y, desde arriba hasta abajo:
 *    Amarillo
 *    Azul
 *    Rojo
 *    Verde   
 * */

#include <Servo.h>

#define BAUD          9600
#define PASOS         200
#define ESPERA        2000

#define M_PRI         0
#define M_SEC         1

#define FULL_STEP     1
#define HALF_STEP     2

#define STEP_MODE     HALF_STEP

#define pinEN         8

#define DELAY         10

// Stop pines
#define pinStopPri    10  // Y-
#define pinStopSec    9   // X-

// Pines y estado del motor paso a paso principal
#define pinPriStep    2
#define pinPriDir     5

// Pines y estado del motor paso a paso secundario
#define pinSecStep    4
#define pinSecDir     7

// Pines de los servos
#define pinPinza      12
#define pinEjeY       13
#define pinRotar      6

Servo sPinza;
Servo sEjeY;
Servo sRotar;

int motores[2]  = { 0, 0 };
int angulo      = 90;
float altura    = 0;
bool abierto    = false;
bool ejecutando = false;

bool iniciando = true;

void setup ()
{
  Serial.begin( BAUD );

  pinMode( pinEN, OUTPUT );
  digitalWrite( pinEN, LOW );

  // Iniciar pines motor principal
  pinMode( pinPriStep, OUTPUT );
  pinMode( pinPriDir, OUTPUT );

  // Iniciar pines motor secundario
  pinMode( pinSecStep, OUTPUT );
  pinMode( pinSecDir, OUTPUT );

  // Inicial pines de los servos
  pinMode( pinPinza, OUTPUT );
  pinMode( pinEjeY, OUTPUT );
  pinMode( pinRotar, OUTPUT );
  pinMode( 2, OUTPUT );

  // Iniciar pines de fin de carrera
  pinMode( pinStopPri, INPUT );
  pinMode( pinStopSec, INPUT );

  sPinza.attach( pinPinza );
  sEjeY.attach( pinEjeY );
  sRotar.attach( pinRotar );

  while ( !Serial );
  cerrarPinza();
}

String comando = "";
void loop ()
{
  if ( Serial && Serial.available() )
  {
    char c = Serial.read();

    if ( c == '\n' )
    {
      procesarComando();
      comando = "";
    }
    else
      comando += c;
  }
  
  while ( motores[0] != 0 || motores[1] != 0 && ejecutando )
  {
    int m0 = 1;
    int m1 = 1;

    int m0abs = abs( motores[0] );
    int m1abs = abs( motores[1] );

    if ( m0abs < 1 )
      m0abs = 1;
    if ( m1abs < 1 )
      m1abs = 1;
    
    if ( m0abs > m1abs )
    {
      m0 = m0abs / m1abs;
    }
    else if ( m0abs < m1abs )
    {
      m1 = m1abs / m0abs;
    }

    if ( motores[0] != 0 )
    {
      digitalWrite( pinPriDir, motores[0] > 0 ? HIGH : LOW );

      for ( int p = 0; p < m0; p++ )
        paso( M_PRI );

      motores[0] += m0 * ( motores[0] < 0 ? 1 : -1 );
    }

    if ( motores[1] != 0 )
    {
      digitalWrite( pinSecDir, motores[1] > 0 ? HIGH : LOW );

      for ( int p = 0; p < m1; p++ )
        paso( M_SEC );

      motores[1] += m1 * ( motores[1] < 0 ? 1 : -1 );
    }
  }

  if ( motores[0] == 0 && motores[1] == 0 && ejecutando )
  {    
    ejecutando = false;
    Serial.write( "!\n" );
  }
}

void ir_inicio ()
{
  digitalWrite( pinPriDir, LOW );
  digitalWrite( pinSecDir, LOW );
  
  while ( !digitalRead( pinStopPri ) || !digitalRead( pinStopSec ) )
  {
    if ( !digitalRead( pinStopPri ) )
      paso( M_PRI );
  
    if ( !digitalRead( pinStopSec ) )
      paso( M_SEC );
  }

  motores[0] = 90 * STEP_MODE * 3;
  motores[1] = 135 * STEP_MODE * 3;
  ejecutando = true;
}

void procesarComando()
{
  String opc = getFirst( comando );
  String params = delFirst( comando );

  if ( opc == "todo" )
  {
    int m1, m2, m3, ab;
    String al;
    params > m1 > m2 > m3 > al > ab;
    
    motores[0] = m1 * STEP_MODE * 3;
    motores[1] = m2 * STEP_MODE * 3;
    angulo = m3;
    altura = al.toFloat();
    abierto = ab == 1;
    
    girar( angulo );
  
    ejeY( altura );
    
    if ( abierto )
      abrirPinza();
    else
      cerrarPinza();

    ejecutando = true;
  }
  else if ( opc == "inicio" )
  {
    ir_inicio();
  }
  else if ( opc == "fin" )
  {
  }
}

void girar ( int angulo )
{
  sRotar.write( angulo );
  delay( DELAY );
}

void ejeY ( float cm )
{
  int v = 180 / 4.5 * cm;
  
  if ( v < 20 )
    v = 20;
    
  sEjeY.write( v );
  delay( DELAY );
}

void abrirPinza ()
{
  sPinza.write( 170 );
  delay( DELAY );
}

void cerrarPinza ()
{
  sPinza.write( 110 );
  delay( DELAY );
}

/* Función: paso
 *  
 *  Params:
 *      - motor: Motor con el que se va a dar un paso
 *  
 *  Descripción: Avanza un paso del motor.
 * */
void paso ( int motor )
{
  // Se selecciona el pinStep del motor pasado por parámetro
  int pinStep = motor == M_PRI ? pinPriStep : pinSecStep;

  // Se manda una señal al controlador
  digitalWrite( pinStep, HIGH );
  delayMicroseconds( ESPERA );
  digitalWrite( pinStep, LOW );
  delayMicroseconds( ESPERA );
}

/*
if ( opc == "m1" )
{
  int angulo;
  params > angulo;
}
else if ( opc == "m2" )
{
  int angulo;
  params > angulo;
}
else if ( opc == "m3" )
{
  int angulo;
  params > angulo;
}
else if ( opc == "al" )
{
  int altura;
  params > altura;
}
else if ( opc == "ab" )
{
  int porcentaje;
  params > porcentaje;
}
else if ( opc == "modo" )
{
  int modo;
  params > modo;
}
*/
