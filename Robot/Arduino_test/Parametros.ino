Param crearParametro ( int ang1, int ang2, int ang3, int altura, int abrir, int tiempo )
{
  Param n = new Nodo;
  
  n->ang1 = ang1;
  n->ang2 = ang2;
  n->ang3 = ang3;
  n->altura = altura;
  n->abrir = abrir;
  n->tiempo = tiempo;

  n->sig = NULL;
  
  return n;
}

void agregarParametro ( Param n )
{
  if ( parametros == NULL )
    parametros = n;
  else
  {
    Param aux = parametros;
    
    while ( aux->sig )
      aux = aux->sig;

    aux->sig = n;
  }
}

void ejecutarParametros ()
{
  if ( parametros != NULL )
  {
    Param p = parametros;
    
    while ( p )
    {
      ejecutar( p );
      p = p->sig;
    }
  }
}
