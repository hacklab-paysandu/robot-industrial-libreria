/* Creado: 21-12-2018
 * Última actualización: 30-7-2019
 * Autor: Jorge Rosas
 * Version: 0.9
 * 
 * Conectar motores a CNC v3:
 * 
 * Motor grande en X, desde arriba hasta abajo:
 *    Verde
 *    Negro
 *    Rojo
 *    Azul0
 * 
 * Motor chico en Y, desde arriba hasta abajo:
 *    Amarillo
 *    Azul
 *    Rojo
 *    Verde   
 * */

#include <Servo.h>

#define BAUD          9600
#define ULTIMO        '\n'

#define PASOS         200
#define ESPERA        2000

#define MOTOR1        0
#define MOTOR2        1
#define MOTOR3        2

#define FULL_STEP     1
#define HALF_STEP     2

#define STEP_MODE     HALF_STEP

#define pinEN         8

#define DELAY         10

#define IZQ           4
#define DER           5

// Stop pines
#define pinStopPri    10  // Y-
#define pinStopSec    9   // X-

// Pines y estado del motor paso a paso principal
#define pinPriStep    2
#define pinPriDir     5

// Pines y estado del motor paso a paso secundario
#define pinSecStep    4
#define pinSecDir     7

// Pines de los servos
#define pinPinza      12
#define pinEjeY       13
#define pinRotar      6

Servo sPinza;
Servo sEjeY;
Servo sRotar;

int motor3Pos = 90;
bool ejecutando = false;

// Estructuras de cómo se guarda cada parámetro recibido
typedef struct Nodo
{
  int ang1;
  int ang2;
  int ang3;
  int altura;
  int abrir;
  int tiempo;
  Nodo* sig;
};

// Definición de parametro singular
typedef Nodo *Param;
// Definición de lista de parametros
typedef Nodo *Parametros;

// Lista de parámetros
Parametros parametros = NULL;

void setup ()
{
  // Se inicia la comunicación con el puerto serial
  Serial.begin( BAUD );
  
  pinMode( pinEN, OUTPUT );
  digitalWrite( pinEN, LOW );

  // Iniciar pines motor principal
  pinMode( pinPriStep, OUTPUT );
  pinMode( pinPriDir, OUTPUT );

  // Iniciar pines motor secundario
  pinMode( pinSecStep, OUTPUT );
  pinMode( pinSecDir, OUTPUT );

  // Inicial pines de los servos
  pinMode( pinPinza, OUTPUT );
  pinMode( pinEjeY, OUTPUT );
  pinMode( pinRotar, OUTPUT );
  pinMode( 2, OUTPUT );

  // Iniciar pines de fin de carrera
  pinMode( pinStopPri, INPUT );
  pinMode( pinStopSec, INPUT );

  sPinza.attach( pinPinza );
  sEjeY.attach( pinEjeY );
  sRotar.attach( pinRotar );

  while ( !Serial );
  cerrarPinza();
  sRotar.write( motor3Pos );
}

String comando = "";
void loop ()
{
  // Mientras haya algo para leer en el puerto
  while ( Serial.available() )
  {
    // Se obtiene un caracter
    char c = ( char ) Serial.read();

    // Si el caracter es igual al ULTIMO caracter del mensaje
    // esto indíca que ya se obtuvo el comando completo
    if ( c == ULTIMO )
    {
      // Se procesa
      procesarComando();
      // Se borra el comando para esperar por el siguiente
      comando = "";
    }
    else
      // Si no es el ULTIMO caracter entonces se concatena al
      // comando para luego procesarlo
      comando += c;
  }
}

void ir_inicio ()
{
  digitalWrite( pinPriDir, LOW );
  digitalWrite( pinSecDir, LOW );
  
  while ( !digitalRead( pinStopPri ) || !digitalRead( pinStopSec ) )
  {
    if ( !digitalRead( pinStopPri ) )
      paso( MOTOR1 );
  
    if ( !digitalRead( pinStopSec ) )
      paso( MOTOR2 );
  }

  ejecutar( crearParametro( 90, 135, 0, 10, 0, 0 ) );
}

/* base: ángulo que se tiene que mover, la función debe retornar cuanto
 * se tiene que mover ese ángulo, con respecto a los otros dos
 * para que lleguen a cero al mismo timepo
 * */
int angulos ( int base, int otro1, int otro2 )
{
  // Si el ángulo base es cero, no se debe mover
  if ( base == 0 )
    return 0;

  // Si el ángulo base es el más chico, se debe mover solo un grado
  if ( base < otro1 && base < otro2 )
    return 1;

  // Si los otros dos ángulos son cero
  // que se mueva todo de una
  if ( otro1 == 0 && otro2 == 0 )
    return base;

  // Si solamente uno de los otros ángulos es cero
  if ( otro1 == 0 || otro2 == 0 )
  {
    // Se obtiene el ángulo más chico
    int noCero = otro1 == 0 ? otro2 : otro1;

    // Si el base es más chico, se mueve solo uno
    if ( base < noCero )
      return 1;
    else
      // De lo contrario se divide entre el otro
      return base / noCero;
  }

  // Si ningúno de los otros dos ángulos es cero
  int chico = otro1 < otro2 ? otro1 : otro2;

  if ( chico < base )
    return base / chico;
  else
    return chico / base;
}

void ejecutar ( Param p )
{
  if ( p->tiempo != 0 )
  {
    delay( p->tiempo );
    return;
  }
  
  // Se obtienen las direcciónes en que se debe mover cada motor
  int dir1 = p->ang1 > 0 ? DER : IZQ;
  int dir2 = p->ang2 > 0 ? DER : IZQ;
  int dir3 = p->ang3 > 0 ? DER : IZQ;
  
  digitalWrite( pinPriDir, dir1 == DER ? HIGH : LOW );
  digitalWrite( pinSecDir, dir2 == DER ? HIGH : LOW );
  
  int pasos1 = p->ang1 * STEP_MODE * 3;
  int pasos2 = p->ang2 * STEP_MODE * 3;
  int pasos3 = p->ang3;
  
  // Se obtiene cuanto se debe mover cáda ángulo en base a los demás
  // para que todos completen su movimiento al mismo tiempo
  int cant1 = angulos( abs( pasos1 ), abs( pasos2 ), abs( pasos3 ) );
  int cant2 = angulos( abs( pasos2 ), abs( pasos1 ), abs( pasos3 ) );
  int cant3 = angulos( abs( pasos3 ), abs( pasos1 ), abs( pasos2 ) );
  
  while ( pasos1 != 0 || pasos2 != 0 || pasos3 || 0 )
  {
    // Se chequea si ya se movieron todos los grados (ang1,ang2,ang3) necesarios
    // si aún quedan grados por mover, se asigna la cantidad de lo contrario se asigna 0
    int mAng1 = pasos1 != 0 ? cant1 : 0;
    int mAng2 = pasos2 != 0 ? cant2 : 0;
    int mAng3 = pasos3 != 0 ? cant3 : 0;

    // Se mueve cada motor en base a la cantidad
    for ( int i1 = 0; i1 < mAng1; i1++ )
      paso( MOTOR1 );

    for ( int i2 = 0; i2 < mAng2; i2++ )
      paso( MOTOR2 );

    for ( int i3 = 0; i3 < mAng3; i3++ )
    {
      motor3Pos += dir3 == DER ? 1 : -1;
      
      if ( motor3Pos < 0 )
        motor3Pos = 0;
      else if ( motor3Pos > 180 )
        motor3Pos = 180;
      
      sRotar.write( motor3Pos );
    }

    // Se reduce la cantidad movida
    pasos1 += dir1 == DER ? -mAng1 : mAng1;
    pasos2 += dir2 == DER ? -mAng2 : mAng2;
    pasos3 += dir3 == DER ? -mAng3 : mAng3;
  }
  
  ejeY( p->altura );
  
  if ( p->abrir )
    abrirPinza();
  else
    cerrarPinza();
}

void mover ( int pin )
{
  digitalWrite( pin, HIGH );
  delay( 100 );
  digitalWrite( pin, LOW );
  delay( 30 );
}

void procesarComando ()
{
  // La variable comando siempre va a tener el siguiente formato
  // opcion:param1:param2: (tantos params como sea necesario en base a la opcion separados por :)

  // Se toma la parte de la opcion
  String opc = getFirst( comando );
  // Se toma la parte de los parametros
  String params = delFirst( comando );

  // Si la opcion es e, esto quiere decir que se ejecuten los
  // parámetros enviados, en este momento
  if ( opc.indexOf( "e" ) != -1 )
  {
    // Si es el primer comando que se ejecuta
    if ( !ejecutando )
    {
      // Se lleva la pinza al inicio
      ir_inicio();
      ejecutando = true;
    }
    // Se obtienen los parámetros
    int ang1, ang2, ang3, altura, abrir;
    params > ang1 > ang2 > ang3 > altura > abrir;

    // Se crea un nodo y se envía a ser ejecutado por la función ejecutar
    ejecutar( crearParametro( ang1, ang2, ang3, altura, abrir, 0 ) );
    // Se indíca que ya terminó
    Serial.print( "!" );
  }
  // Si la opcion es g, esto quiere decir que se guarden los
  // parámetros enviados, para ser ejecutados luego
  else if ( opc.indexOf( "g" ) != -1 )
  {
    // Se obtienen los parámetros
    int ang1, ang2, ang3, altura, abrir;
    params > ang1 > ang2 > ang3 > altura > abrir;

    // Se crea un nodo y se guarda
    agregarParametro( crearParametro( ang1, ang2, ang3, altura, abrir, 0 ) );
    // Se indíca que ya terminó
    Serial.print( "!" );
  }
  // Si la opción es r, esto quiere decir que se ejecuten los
  // parámetros previamente enviados
  else if ( opc.indexOf( "r" ) != -1 )
  {
    // Se indíca que se recibió la acción
    // esto se hace antes de ejecutarlos para que el programa
    // en c, que este corriendo pueda seguir con otras tareas
    // mientras los parámetros son ejecutados
    Serial.print( "!" );
    
    // Se lleva la pinza al inicio
    ir_inicio();
    
    // Se procede a ejecutarlos
    ejecutarParametros();
  }
  // La opción t, indica tiempo de espera
  else if ( opc.indexOf( "t" ) != -1 )
  {
    int tiempo;
    char modo;
    params > modo > tiempo;

    Serial.println( modo );

    // Modo g = guardar, e = ejecutar ahora
    if ( modo == 'e' ) 
      delay( tiempo );
    else
      agregarParametro( crearParametro( 0, 0, 0, 0, 0, tiempo ) );
    
    Serial.print( "!" );
  }
  // La opción f, indica si se terminaron de ejecutar los parametros
  else if ( opc.indexOf( "f" ) != -1 )
  {
    ejecutando = false;
  }
}

void girar ( int angulo )
{
  sRotar.write( angulo );
}

void ejeY ( float cm )
{
  int v = 180 / 4.5 * cm;
  
  if ( v < 20 )
    v = 20;
    
  sEjeY.write( v );
}

void abrirPinza ()
{
  sPinza.write( 180 );
}

void cerrarPinza ()
{
  sPinza.write( 100 );
}

/* Función: paso
 *  
 *  Params:
 *      - motor: Motor con el que se va a dar un paso
 *  
 *  Descripción: Avanza un paso del motor.
 * */
void paso ( int motor )
{
  // Se selecciona el pinStep del motor pasado por parámetro
  int pinStep = motor == MOTOR1 ? pinPriStep : pinSecStep;

  // Se manda una señal al controlador
  digitalWrite( pinStep, HIGH );
  delayMicroseconds( ESPERA );
  digitalWrite( pinStep, LOW );
  delayMicroseconds( ESPERA );
}
