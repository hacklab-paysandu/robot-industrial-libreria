#!/usr/bin/env python
from tkinter import *
from tkinter import messagebox
import os
import pyperclip

def get_puertos () :
	# Obtiene dispositivos listados como USB
	devs = os.popen( 'find /sys/bus/usb/devices/usb*/ -name dev' ).read()
	ports = []

	# Por cada uno de ellos se chequea si aparece en su directorio /ttyUSB o /ttyACM
	# Dispositivos arduino van a aparecer con una de estas dos
	for d in devs.split( '\n' ) :
		if '/ttyUSB' in d or '/ttyACM' in d :
			# Si un dispositivo tiene de nombre /dev/ttyUSB0, entonces las ultimas
			# dos partes de su ruta en devs será /ttyUSB0/dev

			# Se obtinen las partes
			partes = d.split( '/' )
			# Se colocan en orden inverso
			partes.reverse()
			# Se forma el nombre del puerto en base a las partes de la ruta
			puerto = f'/{partes[0]}/{partes[1]}'

			# Se obtiene informacion sobre el puerto y con grep solamente aparecera la linea que tenga el ID_SERIAL
			id_serial = os.popen( f'udevadm info -q property --export -p {d[:-4]} | grep ID_SERIAL' ).read()
			# Se limpia el formato ID_SERIAL='contenido', para solamente obtener el string del contenido
			# El ID_SERIAL contiene el nombre que se le dio al dispositivo de fabrica, esto ayuda a reconocerlo visualmente
			id_serial = id_serial.replace( 'ID_SERIAL=', '' ).replace( '\'', '' ).replace( '\n', '' )

			# Se agrega el puerto con el formato: puerto (nombre)
			ports.append( f'{puerto} ({id_serial})' )

	# Se devuelven todos los encontrados
	return ports

def actualizar () :
	# Vacia la lista de puertos
	lista.delete( 0, END )

	# Obtiene los puertos y los agrega
	for p in get_puertos() :
		lista.insert( END, p )

def copiar () :
	# Se obtiene la seleccion actual, esto siempre es una lista, por mas que solo se seleccione uno
	seleccionados = lista.curselection()

	if len( seleccionados ) == 0 : # Si no hay seleccionados
		messagebox.showinfo( 'Copiado', 'Seleccione un puerto de la lista para copiarlo' )
	else :
		# De el primer seleccionado, se separa por espacio (el formato es: puerto (nombre)), y obtenemos el primer valor, esto nos da el nombre del puerto
		puerto = lista.get( seleccionados[0] ).split( ' ' )[0]
		# Se copia al porta papeles
		pyperclip.copy( puerto )
		# Se muestra mensaje
		messagebox.showinfo( 'Copiado', f'{puerto} copiado al portapapeles' )

ventana = Tk() # Se crea la ventana
ventana.title( 'Puertos conectados' )
ventana.resizable( False, False )

# Colocar una lista con un scroll bar vertical
f = Frame( ventana )
scrollbar = Scrollbar( f, orient = VERTICAL )
lista = Listbox( f, yscrollcommand = scrollbar.set )
lista.config( width = 60 )
scrollbar.config( command = lista.yview )
scrollbar.pack( side = RIGHT, fill = Y )
lista.pack( side = LEFT, fill = BOTH, expand = 1 )
f.pack()

# Se crean los dos botones
Button( ventana, text = 'Copiar', command = copiar ).pack()
Button( ventana, text = 'Actualizar', command = actualizar ).pack()

actualizar() # Se actualiza la lista de puertos

mainloop() # Se inicia el loop de la aplicacion
